```
#!text
      ______                               _   ___ _____ 
      |  _  \                             | | |_  /  ___|
      | | | |___ _ __ ___   __ _ _ __   __| |   | \ `--. 
      | | | / _ \ '_ ` _ \ / _` | '_ \ / _` |   | |`--. \
      | |/ /  __/ | | | | | (_| | | | | (_| /\__/ /\__/ /
      |___/ \___|_| |_| |_|\__,_|_| |_|\__,_\____/\____/                     
              +-+-+-+-+-+ +-+-+ +-+-+-+-+-+-+
              |D|o|i|n|g| |i|t| |r|i|g|h|t|!|
              +-+-+-+-+-+ +-+-+ +-+-+-+-+-+-+
```

To use DemansJs you need to load it like you would with any script:


```
#!html

<script type="text/javascript" src="demandjs.js"></script>
```


## API: ##
* demandjs.config
* demandjs.include()
* demandjs.clearCache()

### demandjs.include() ###

```
#!javascript

demandjs.include('handle', ['demand'], callback);
//Alternative, if we have demands that depend on other scripts
demandjs.include('handle', [{'handle':'handelName', 'demand':'handelName'}], callback);
```
Fetches a single script and executes it. If the script is cached it will be loaded from localStorage, otherwise it will be loaded from the network. If the script do not depend on any other scripts it will be injected into the document for the browser to execute. If any dependencies exists, it will wait for them to execute first.

* **handle** (*required*) The name of the script we want to load, will he the demand handler if other scripts depend on it.
* **demand** (*optional*) If the script dependents on one or more scripts we put their handler name here as an array.
* **callback** (*optional*) Relative or full path to the script we want to load.

### demandjs.config ###

```
#!javascript

demandjs.config = {
  cache: true,
  basePath: http://localhost/js/
};
```
Configure how DemandJs should work.

* **cache** true or false. Defaults is set to true.
* **basePath** Default path is set to where demandjs.js is located.

### demandjs.clearCache()###

```
#!javascript

demandjs.clearCache();
```
Clears all cached scripts.

## Example: ##

```
#!javascript

demandjs.include('setup', []);
demandjs.include('front',
  ['setup'],
  function(){
    console.log('I am executed when the script is loaded.');
  });
```