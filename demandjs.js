/**
 * @name DemandJs
 * @version 1.0
 * @author Jamie Telin (jamie.telin@gmail.com)
 * @license http://www.gnu.org/licenses/lgpl.html  
 * @example
 *    // Set cache on or off
 *    demandjs.config.cache = false;
 *    // Change basePath, defaults to path where demandjs.js is
 *    demandjs.config.basePath = http://localhost/js/;
 *    // Loads database.js
 *    demandjs.include('database', 'database.js');
 *    // Waits untill admin.js is loaded before its loaded
 *    demandjs.include('users', ['admin'], 'users.js');
 *    // Loads after database
 *    demandjs.include('admin', ['database'],'admin.js');
 */
var demandjs = (function demandjs()
{
  
  var _scriptsQueue = new Array();
  var _scriptsLoaded = new Array();
  var _xmlhttp;
  var config =  {
    cache: true,
    basePath: getBasePath() // This defaults to current demandjs.js path
  };

  document.addEventListener('demandjs_include', handleEvents);
  document.addEventListener('demandjs_loaded', handleEvents);
  
  function handleEvents(e)
  {
    switch(e.type) {
      case 'demandjs_include':
        
        // Run the queue and see if we can include anything
        doIncludeQueue(e.detail);
        
      break;
      case 'demandjs_loaded':
        
        // Remove script from queue
        removeFromQueue(e.detail.handler);
        
        // To speed things up we can try to cache the script
        cacheIt(e.detail);
        
        // Run any callback functions
        if(typeof e.detail.callback !== 'undefined') {
          e.detail.callback();
        }
        
        // Run the queue again to pick up any new scripts that can be added now
        doIncludeQueue(e.detail);
        
      break;
    }
  }
  
  /**
   * Gets the basePath based on where demandjs.js is located
   * 
   * @return {string}
   * @since 1.0
   */
  function getBasePath()
  {
    var scripts = document.getElementsByTagName("script"),
    src = scripts[scripts.length-1].src;
    return src.replace(/[^\/]*$/, '');
  }
  
  /**
   * Checks if a path is relative or not
   * 
   * @param path
   * @return {boolean}
   * @since 1.0
   */
  function isRelativePath(path)
  {
    if(path.indexOf('http://') > -1 ||
       path.indexOf('https://') > -1 ||
       path.indexOf('//') > -1) {
      return false;
    }
    return true;
  }
  
  /**
   * Caches scripts if we have localStorage support
   * 
   * @param includeObj
   * @use supportsLocalStorage()
   * @use post
   * @since 1.0
   */
  function cacheIt(includeObj)
  {
    // Check if we have localStorage support
    if (supportsLocalStorage() && config.cache) {
      // Get the script source code
      post({}, includeObj.path, function(r){
        // Save it
        localStorage["demandjs_script_" + includeObj.handler] = r;
      });
    }
  }
  
  /**
   * Cleans up any cached scripts
   * 
   * @use supportsLocalStorage()
   * @since 1.0
   */
  var clearCache = function clearCache()
  {
    if (supportsLocalStorage()) {
      for(var key in localStorage) {
        if(key.indexOf("demandjs_script_") > -1) {
          localStorage.removeItem(key);
        }
      }
    }
  };
  
  /**
   * Checks if a handler/s is loaded
   * 
   * @param handler
   *    Might be string or array of handlers
   * @return  {boolean}
   * @since 1.0
   */
  function isLoaded(handler) {
    var loaded = 0;
    var handlers = new Array();
    
    if(typeof handler === 'string') {
      handlers.push(handler);
    } else {
      handlers = handler;
    }
    if(handlers.length === 0) {
      return true;
    }
    
    for(var key in handlers) {
      if(typeof handlers[key] === 'string') {
        handlers[key] = {
          'handler': handlers[key],
          'demand': []
        };
      }
    }
    
    for(var i = 0; i < _scriptsLoaded.length; i++) {
      
      for(var x = 0; x < handlers.length; x++) {
        if(_scriptsLoaded[i].handler === handlers[x].handler) {
          loaded++;
        }
      }
    }

    if(handlers.length === loaded) {
      return true;
    }
    return false;
  }
  
  /**
   * Remove a handler from queue
   * 
   * @param handler
   * @return {boolean}
   *   true or false depending on whether it was removed
   * @since 1.0
   */
  function removeFromQueue(handler) {

    for(var i = 0; i < _scriptsQueue.length; i++) {
      
      if(_scriptsQueue[i].handler === handler) {
        _scriptsLoaded.push(_scriptsQueue[i]);
        _scriptsQueue.splice(i, 1);
        return true;
      }
      
    }
    
    return false;
    
  }
  
  /**
   * Checks if a handler/s is loaded
   * 
   * @param handler
   *    Might be string or array of handlers
   * @return  {boolean}
   * @since 1.0
   */
  function getIncludeObj(handler) {
    
    for(var i = 0; i < _scriptsQueue.length; i++) {

      if(_scriptsQueue[i].handler === handler) {
        return _scriptsQueue[i];
      }
      
    }

    return false;
  }
  
  /**
   * Does the acctual injecting of script tags in the header
   * 
   * @param includeObj
   * @return {array}
   *   array of includeObjs
   * @since 1.0
   */
  function doIncludeQueue(includeObj)
  {
    // Go through queue and include any if demands are meet
    for(var i = 0; i < _scriptsQueue.length; i++) {
      if(isLoaded(_scriptsQueue[i].demand)) {
        
        if (supportsLocalStorage() && config.cache) {
          cachedScript = localStorage["demandjs_script_" + _scriptsQueue[i].handler];
          
          if(typeof cachedScript !== 'undefined') {
             
            _scriptsQueue[i].script.removeAttribute('src');
            document.head.appendChild(_scriptsQueue[i].script);
            _scriptsQueue[i].script.text = cachedScript;
            
            var event = new CustomEvent('demandjs_loaded',
            { 'detail': _scriptsQueue[i] });
            document.dispatchEvent(event);
            
            return;
          }
        }
        
        document.head.appendChild(_scriptsQueue[i].script);
        
        _scriptsQueue[i].script.onload = function(){
          var event = new CustomEvent('demandjs_loaded',
            { 'detail': getIncludeObj(this.dataset['handler']) });
          document.dispatchEvent(event);
        };
        
      }
    }

    return;
  }
  
  function inQueueOrLoaded(handler)
  {
    if(isLoaded(handler)) {
      return true;
    } 
    else if(getIncludeObj(handler)) {
      return true;
    }
    
    return false;
  }
  
  /**
   * Public function to include a js-file
   * 
   * Also checks for dependency
   * 
   * @param {string} handler
   * @param {array} demand
   * @param {string} path
   * @since 1.0
   */
  var include = function(handler, demand, callback)
  {
    var script, path, includeObj;
    
    // If allready loaded or in queue we bail...
    if(inQueueOrLoaded(handler)) {
      return;
    }

    path = handler;
    
    if(isRelativePath(path)) {
      path = config.basePath + path;
    }
    
    // Handler do not need to have a file extension
    // so we check if it has, else we add it
    if(path.indexOf('.js') === -1) {
      path = path + '.js';
    }
    
    // Create out script element
    script = document.createElement('script');
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', path);
    script.setAttribute('data-handler', handler);
    
    // We also want to set a dataset for demands
    // as an json string
    if(demand.length !== 0) {
      var demandJsonArray = new Array();
      demand.forEach(function(value, index) {
        demandJsonArray.push('"'+value+'"');
      });
      script.setAttribute('data-demand', '[' + demandJsonArray.join(', ') + ']');
    }
    
    
    // Create our includeObj that is used in event and _scripts array
    includeObj = {
      'handler': handler,
      'path': path,
      'demand': demand,
      'script': script,
      'callback': callback
    };
    
    // Push our includeObj to _scripts array
    _scriptsQueue.push(includeObj);
    
    // Create demandjs_include event
    var event = new CustomEvent('demandjs_include', { 'detail': includeObj });
    document.dispatchEvent(event);
    
    // Here we autoload any demands from scripts we have
    for(var key in demand) {
      
      if(typeof demand[key] === 'string') {
        demand[key] = {
          'handler': demand[key],
          'demand': []
        };
      }
      
      // If not allready loaded or in queue
      // we include it now
      if(!inQueueOrLoaded(demand[key].handler)) {
        include(demand[key].handler, demand[key].demand);
      }
    }
    
  };
  
  /**
   * Ajax post
   * 
   * Allows us to use ajax post to send and recive data
   * 
   * @param {object} data
   * @param {string} url
   * @param {function} callback
   * @since 1.0
   */
  function post(data, url, callback) {
    
    var dataString = "";
    for (var key in data) {
        if (dataString != "") {
            dataString += "&";
        }
        dataString += key + "=" + data[key];
    }
    
    _xmlhttp.onreadystatechange = function() {
      if (_xmlhttp.readyState==4 && _xmlhttp.status==200) {
        callback(_xmlhttp.responseText);
      }
    };
    
    _xmlhttp.open("POST", url, true);
    _xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    _xmlhttp.send(dataString);
  }
  
  // Warm up Ajax
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    _xmlhttp = new XMLHttpRequest();
  } else {
    // code for IE6, IE5
    _xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  
  function supportsLocalStorage()
  {
    try {
      return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
      return false;
    }
  }
  
  /**
   * Polyfill CustomEvent() for Internet Explorer 9 and 10
   *
   * @link https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent
   */
  (function(){
    /**
     * Polyfill CustomEvent() for Internet Explorer 9 and 10
     *
     * @param {object} event
     * @param {object} params
     */
    function CustomEvent (event, params)
    {
      params = params || { bubbles: false, cancelable: false, detail: undefined };
      var evt = document.createEvent('CustomEvent');
      evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
      return evt;
    };
    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
  })();
  
  /**
   * @name HTML 5 dataset Support
   * @version 0.0.2
   * @home http://code.eligrey.com/html5/dataset/
   * @author Elijah Grey - eligrey.com
   * @license http://www.gnu.org/licenses/lgpl.html
   */
  (function(){
    function toCamelCase(str)
    {
        return str.replace(/\-./g, function(substr){ return substr.charAt(1).toUpperCase();});
    }

    Element.prototype.setDataAttribute = function(name, value) {
      if ( value !== undefined ) return this.setAttribute('data-'+name, value);
      else return this.removeDataAttribute(name);
    };
    Element.prototype.removeDataAttribute = function(name) {
      return this.removeAttribute('data-'+name);
    };
    Element.prototype.setDataAttributes = function(items) {
      if ( items instanceof Object ) {
        for (attr in items) if ( items.hasOwnProperty(attr) ) this.setDataAttribute(attr, items[attr]);
      }
    };
    if ( !Element.prototype.__lookupGetter__("dataset") ) {
      Element.prototype.__defineGetter__("dataset", function() {
        try { // simulate DOMStringMap w/accessor support
          var getter_test = {};
          getter_test.__defineGetter__("test", function(){}); // test setting accessor on normal object
          delete getter_test;
          var HTML5_DOMStringMap = {};
        } catch(e) { var HTML5_DOMStringMap = document.createElement("div"); } // use a DOM object for IE8
        function lambda(o) { return function(){return o} };
        function dataSetterFunc(ref_el, attrName) { return function(val){ return ref_el.setDataAttribute(attrName, val) } };
        for ( attr in this.attributes ) {
          if ( this.attributes.hasOwnProperty(attr) && this.attributes[attr].name && /^data-[a-z_\-\d]*$/i.test(this.attributes[attr].name) ) {
            var attrName = toCamelCase(this.attributes[attr].name.substr(5)), attrVal = this.attributes[attr].value;
            try {
                    HTML5_DOMStringMap.__defineGetter__(attrName, lambda(attrVal || '') );
                    HTML5_DOMStringMap.__defineSetter__(attrName, dataSetterFunc(this, attrName) );
            }
            catch (e) { HTML5_DOMStringMap[attrName] = attrVal } // if accessors are not working
          }
        }
        return HTML5_DOMStringMap;
      });
    }
  })();
  
  // Public scope
  return {
    include: include,
    clearCache: clearCache,
    scriptsQueue: _scriptsQueue,
    scriptsLoaded: _scriptsLoaded,
    config: config
  };
  
})();
